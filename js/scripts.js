const btn = document.getElementById('generator');
const pallettes = document.getElementsByClassName('pallette');
const colors_based = [1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F'];

btn.addEventListener('click',function(e){
  e.preventDefault();

  for( let i=0; i < pallettes.length; i++ ){
    let j = 0;
    let color = '';
    for( j=0; j < 6; j++ ){
      color += colors_based[Math.floor(Math.random() * 15)];
    }
    console.log(color);
    pallettes[i].style.backgroundColor = `#${color}`;
    pallettes[i].nextElementSibling.innerHTML = `#${color}`;
  }
});
